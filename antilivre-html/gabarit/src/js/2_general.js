// Scripts
const fabrique = document.querySelector('.button--shuffle');
const clearBtn = document.querySelector('.button--clear');
const infoBtn = document.querySelector('.button--info');
const paysageBtn = document.querySelector('.button--paysage');
const info = document.querySelector('.informations');
const options = document.querySelector('.options');
const contenu = document.querySelector('.contenu');
const contenuDessin = document.querySelector('.contenu__image--visible');
const dessins = Array.from(document.querySelectorAll('.contenu__image img'));

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
var content = document.querySelector('.contenu');
content.addEventListener('touchstart', function (event) {
    this.allowUp = (this.scrollTop > 0);
    this.allowDown = (this.scrollTop < this.scrollHeight - this.clientHeight);
    this.slideBeginY = event.pageY;
});

content.addEventListener('touchmove', function (event) {
    var up = (event.pageY > this.slideBeginY);
    var down = (event.pageY < this.slideBeginY);
    this.slideBeginY = event.pageY;
    if ((up && this.allowUp) || (down && this.allowDown)) {
        event.stopPropagation();
    }
    else {
        event.preventDefault();
    }
});

// Shuffle array
function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

function Affiche() {
  contenuDessin.innerHTML = "";
  shuffleArray(dessins);
  let dessin = dessins[0];
  contenuDessin.appendChild(dessin);
}

// Custom cursor
const cursor = document.querySelector('.cursor--main');
const cursorBg = document.querySelector('.cursor--bg');
const links = document.querySelectorAll('a, input, .button');

let cursorX = -100;
let cursorY = -100;
let cursorBgX = -100;
let cursorBgY = -100;
let pageLoad = true;

const cursorMove = () => {
  document.addEventListener('mousemove', (e) => {
    cursorX = e.clientX;
    cursorY = e.clientY;
    if (pageLoad) {
      cursorBgX = e.clientX;
      cursorBgY = e.clientY;
      pageLoad = !pageLoad;
    }
  });

  // Lerp (linear interpolation)
  const lerp = (start, finish, speed) => {
    return (1 - speed) * start + speed * finish;
  };

  const rendering = () => {
    cursorBgX = lerp(cursorBgX, cursorX, 0.1);
    cursorBgY = lerp(cursorBgY, cursorY, 0.1);
    cursor.style.left = `${cursorX}px`;
    cursor.style.top = `${cursorY}px`;
    cursorBg.style.left = `${cursorBgX}px`;
    cursorBg.style.top = `${cursorBgY}px`;
    requestAnimationFrame(rendering);
  };
  requestAnimationFrame(rendering);
}

cursorMove();

// Interaction with the cursor
 links.forEach((e) => {
   e.addEventListener('mouseenter', () => {
     cursor.classList.add('cursor--interact');
   });
   e.addEventListener('mouseleave', () => {
     cursor.classList.remove('cursor--interact');
   });
 });

 document.addEventListener('click', () => {
   cursor.classList.add('cursor--click');

   setTimeout(() => {
     cursor.classList.remove('cursor--click');
   }, 250);
 });

fabrique.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.remove('informations--show');
  Affiche();
});

infoBtn.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.toggle('informations--show');
});


let downloadImage = document.querySelector('.button--img');
downloadImage.addEventListener('click', (e) => {
  e.preventDefault();
  options.classList.add("hide");
  document.documentElement.classList.add("hide-scrollbar");

  html2canvas(document.querySelector('.contenu'),{
    allowTaint: false,
    logging: false,
    backgroundColor: "#ffffff",
    letterRendering: true,
    scale: 2
  }).then(function(canvas) {
    // const link = document.createElement('a');
    // document.body.appendChild(link);
    // link.download = 'dio.jpg';
    // link.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
    // link.target = '_blank';
    // link.click();
    saveAs(canvas.toDataURL("image/jpeg"), 'come-un-soffio.jpg');
  });

  document.documentElement.classList.remove("hide-scrollbar");
  options.classList.remove("hide");
});


function saveAs(uri, filename) {
  const link = document.createElement('a');

  if (typeof link.download === 'string') {
    link.href = uri;
    link.download = filename;
    link.target = '_blank';
    document.body.appendChild(link);
    link.click();
    //remove the link when done
    document.body.removeChild(link);
  } else {
    window.open(uri);
  }
}

// Canvas p5.js

const optionsHeight = document.querySelector('.options').clientHeight;
const eraserBtn = document.querySelector('.button--eraser');
let eraserToggle = false;
let crayon;
let gomme;
const brushSize = 10;

eraserBtn.addEventListener('click', (e) => {
  e.preventDefault();
  if (!eraserToggle) {
    eraserBtn.style.background = "#000";
    eraserBtn.style.color = "#fff";
    cursor.classList.toggle('cursor--eraser');
    cursorBg.classList.toggle('cursor--eraser');
    eraserToggle = !eraserToggle;
  } else if (eraserToggle) {
    eraserBtn.style.background = "#fff";
    eraserBtn.style.color = "#000";
    cursor.classList.toggle('cursor--eraser');
    cursorBg.classList.toggle('cursor--eraser');
    eraserToggle = !eraserToggle;
  }
});

// function preload() {
//   crayon = loadImage(brush);
//   gomme = loadImage(eraser);
// }

function setup() {
  const canvas = createCanvas(windowWidth, windowHeight);
  canvas.parent('contenu');
}

function draw() {
  if (mouseIsPressed) {
    if (mouseY > optionsHeight) {
      if (eraserToggle) {
        // imageMode(CENTER);
        // image(gomme, mouseX, mouseY, brushSize, brushSize);
        stroke(255,255,255);
        strokeWeight(10);
        line(pmouseX, pmouseY, mouseX, mouseY);
      } else if (!eraserToggle) {
        // imageMode(CENTER);
        // image(crayon, mouseX, mouseY, brushSize, brushSize);
        stroke(0,0,0);
        strokeWeight(2);
        line(pmouseX, pmouseY, mouseX, mouseY);
      }
    }
  }
}

// function mouseDragged() {
//   strokeWeight(2);
//   line(pmouseX, pmouseY, mouseX, mouseY);
// }

clearBtn.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.remove('informations--show');
  clear();
});

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

Affiche();

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});
