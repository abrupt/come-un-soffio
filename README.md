# ~/ABRÜPT/LUCIA PIGLIAPOCHI/COME UN SOFFIO/*

La [page de ce livre](https://abrupt.cc/lucia-pigliapochi/comeunsoffio/) sur le réseau.

## Sur l'antilivre

*Zine à dessiner. Zine à occuper.*

"Come un soffio" est une collection de dessins trouvés entre deux carnets. Des dessins rapides à l'ombre pour comprendre ce qui se passe à l'intérieur et à l'extérieur de l'autrice.

*Zine da disegno. Zine da occupare.*

"Come un soffio" è una collezione di disegni trovati tra fogli di appunti. Disegni in ombra e veloci per capire cosa succede dentro e fuori e lasciarlo impresso.


## Sur l'autrice

Née en 1984 à Côme, en Italie. Elle a vécu à Urbino et à Barcelone. Elle vit et travaille actuellement à Zürich. Elle dessine, écrit de la poésie et est co-fondatrice de Franz Collective.

Nata nel 1984 a Como, in Italia. Ha vissuto a Urbino e Barcellona. Attualmente vive e lavora a Zurigo. Disegna, scrive poesie ed é cofondatrice di Franz Collective.

[www.luciapigliapochi.com](https://www.luciapigliapochi.com/)


## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
